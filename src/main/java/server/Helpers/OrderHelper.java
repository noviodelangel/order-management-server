package server.Helpers;

import server.database.model.*;
import server.database.model.services.*;
import server.model.Order;
import server.model.OrderStatus;
import server.model.Status;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sosna on 09.11.2015.
 */
public class OrderHelper {

    private OrdersService _ordersService;
    private OrderBasketsService _orderBasketsService;
    private AddressesService _addressService;
    private CompaniesService _companiesService;
    private DishDetailsService _dishDetailsService;
    private DishesService _dishService;

    public OrderHelper(OrdersService ordersService, OrderBasketsService orderBasketsService, AddressesService addressesService, CompaniesService companiesService, DishDetailsService dishDetailsService, DishesService dishService){
        _ordersService = ordersService;
        _orderBasketsService = orderBasketsService;
        _addressService = addressesService;
        _companiesService = companiesService;
        _dishDetailsService = dishDetailsService;
        _dishService = dishService;
    }

    public String addOrder(Order orderWithAddress, int customerId, int companyId){
        long now = new Date().getTime();

        Addresses address = orderWithAddress.getAddress();
        OrderBaskets[] orderBaskets = orderWithAddress.getOrderedItems();

        Orders order = new Orders();
        order.setCompanyId(companyId);
        order.setCustomerId(customerId);
        order.setOrderDate(new Timestamp(now));
        order.setStatus(Status.ORDERED.getValue());

        int addressId;
        if (!isCustomerAddress(address, customerId)) {
            addressId = _addressService.save(address);
        }
        else {
            addressId = _addressService.getAddressByCustomerId(customerId).getId();
        }

        order.setAddressId(addressId);

        int orderId = _ordersService.save(order);

        for(OrderBaskets item : orderBaskets){
            item.setOrderId(orderId);
            _orderBasketsService.save(item);
        }

        return "Zamówienie zostało dodane";
    }

    public List<OrderStatus> getCustomerOrderStatuses(int customerId) {

        List<Orders> orders = _ordersService.getByCustomerId(customerId);
        List<OrderStatus> orderStatuses = new ArrayList<>();

        for(Orders order : orders) {
            List<OrderBaskets> orderBaskets =  _orderBasketsService.getByOrderId(order.getId());
            OrderStatus orderStatus = new OrderStatus();

            orderStatus.setDeliveryDate(order.getDeliveryDate());
            orderStatus.setStatus(order.getStatus());

            for (OrderBaskets basket : orderBaskets) {
                DishDetails dishDetails = _dishDetailsService.getById(basket.getDishId());
                Dishes dish = _dishService.getById(dishDetails.getDishId());
                orderStatus.setPayment(orderStatus.getPayment() + dishDetails.getPrice() * basket.getAmount());

                orderStatus.setDetails(orderStatus.getDetails() + basket.getAmount() + "x " + dish.getName() + " ");
            }

            Companies company = _companiesService.getById(order.getCompanyId());

            orderStatus.setCompanyName(company.getName());

            orderStatuses.add(orderStatus);
        }
        return orderStatuses;
    }

    private Boolean isCustomerAddress(Addresses address, int customerId){
        Addresses customerAddress = _addressService.getAddressByCustomerId(customerId);

        if (customerAddress.getCity() == address.getCity() &&
            customerAddress.getStreet() == address.getStreet() &&
            customerAddress.getHouseNumber() == address.getHouseNumber() &&
            customerAddress.getPostalCode() == address.getPostalCode()){

            return true;
        }
        return false;
    }
}
