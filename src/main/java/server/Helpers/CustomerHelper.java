package server.Helpers;

import server.database.model.Customers;
import server.database.model.services.CustomersService;

import java.security.MessageDigest;

/**
 * Created by sosna on 18.10.2015.
 */
public class CustomerHelper {

    public static boolean verify(int customerId, String sessionNumber, CustomersService customersService){
        Customers customer = customersService.getById(customerId);

        if (customer.getSessionNumber().equals(sessionNumber)){
            return true;
        }
        return false;
    }

    public static Customers login(String email, String password, CustomersService customersService){
        Customers customer = customersService.getByAttribute("email", email);

        if (customer != null && customer.getPassword().equals(password)){
            String sessionNumber = generateSessionNumber(email);
            customer.setSessionNumber(sessionNumber);

            customersService.update(customer);

            return customer;
        }
        Customers errorCustomer = new Customers();
        errorCustomer.setSessionNumber("0");

        return errorCustomer;
    }

    public static String generateSessionNumber(String email){
        return hash(email + 11711 + Math.random() * 26723 + System.currentTimeMillis());
    }

    public static String hash(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}
