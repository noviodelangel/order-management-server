package server.database.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by sosna on 12.10.2015.
 */
@Entity
@Proxy(lazy = false)
public class Companies {
    private int id;
    private String name;
    private String description;
    private Boolean status;
    private Timestamp openDate;
    private Timestamp closeDate;
    private String email;
    private String telephone;
    private String website;
    private int addressId;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "status")
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Basic
    @Column(name = "open_date")
    public Timestamp getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Timestamp openDate) {
        this.openDate = openDate;
    }

    @Basic
    @Column(name = "close_date")
    public Timestamp getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Timestamp closeDate) {
        this.closeDate = closeDate;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "telephone")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "address_id")
    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Companies companies = (Companies) o;

        if (id != companies.id) return false;
        if (status != companies.status) return false;
        if (addressId != companies.addressId) return false;
        if (name != null ? !name.equals(companies.name) : companies.name != null) return false;
        if (description != null ? !description.equals(companies.description) : companies.description != null)
            return false;
        if (openDate != null ? !openDate.equals(companies.openDate) : companies.openDate != null) return false;
        if (closeDate != null ? !closeDate.equals(companies.closeDate) : companies.closeDate != null) return false;
        if (email != null ? !email.equals(companies.email) : companies.email != null) return false;
        if (telephone != null ? !telephone.equals(companies.telephone) : companies.telephone != null) return false;
        if (website != null ? !website.equals(companies.website) : companies.website != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (openDate != null ? openDate.hashCode() : 0);
        result = 31 * result + (closeDate != null ? closeDate.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + addressId;
        return result;
    }
}
