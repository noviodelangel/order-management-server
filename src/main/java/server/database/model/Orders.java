package server.database.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by sosna on 12.10.2015.
 */
@Entity
@Proxy(lazy = false)
public class Orders {
    private int id;
    private int customerId;
    private int status;
    private Timestamp orderDate;
    private Timestamp deliveryDate;
    private int companyId;
    private int addressId;
    private int isShown;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "customer_id")
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "order_date")
    public Timestamp getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Timestamp orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "delivery_date")
    public Timestamp getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Timestamp deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @Basic
    @Column(name = "company_id")
    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "address_id")
    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "is_shown")
    public int getIsShown() {
        return isShown;
    }

    public void setIsShown(int isShown) {
        this.isShown = isShown;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (id != orders.id) return false;
        if (customerId != orders.customerId) return false;
        if (status != orders.status) return false;
        if (companyId != orders.companyId) return false;
        if (addressId != orders.addressId) return false;
        if (isShown != orders.isShown) return false;
        if (orderDate != null ? !orderDate.equals(orders.orderDate) : orders.orderDate != null) return false;
        if (deliveryDate != null ? !deliveryDate.equals(orders.deliveryDate) : orders.deliveryDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + customerId;
        result = 31 * result + status;
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        result = 31 * result + (deliveryDate != null ? deliveryDate.hashCode() : 0);
        result = 31 * result + companyId;
        result = 31 * result + addressId;
        result = 31 * result + isShown;
        return result;
    }
}
