package server.database.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * Created by sosna on 12.10.2015.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "dish_details", schema = "", catalog = "ordersdb")
public class DishDetails {
    private int id;
    private int dishId;
    private String size;
    private float price;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dish_id")
    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    @Basic
    @Column(name = "size")
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Basic
    @Column(name = "price")
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DishDetails that = (DishDetails) o;

        if (id != that.id) return false;
        if (dishId != that.dishId) return false;
        if (Float.compare(that.price, price) != 0) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + dishId;
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        return result;
    }
}
