package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.Companies;

import java.util.List;

@Repository
@Transactional
public class CompaniesService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Companies companies) {
        return (Integer) getSession().save(companies);
    }

    public void delete(Companies company) {
        getSession().delete(company);
    }

    @SuppressWarnings("unchecked")
    public List<Companies> getAll() {
        return getSession().createQuery("from Companies").list();
    }

    public Companies getByAttribute(String name, String value) {
        return (Companies) getSession().createQuery(
                "from companies where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Companies getById(int id) {
        return (Companies) getSession().load(Companies.class, id);
    }

    public void update(Companies company) {
        getSession().update(company);
    }
}
