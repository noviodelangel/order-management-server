package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.DishDetails;
import server.database.model.Dishes;
import server.model.Menu;
import server.model.MenuDetails;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class DishesService {

    @Autowired
    private SessionFactory _sessionFactory;
    @Autowired
    private DishDetailsService _dishDetailsService;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Dishes dish) {
        return (Integer) getSession().save(dish);
    }

    public void delete(Dishes dish) {
        getSession().delete(dish);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from Dishes").list();
    }

    public Dishes getByAttribute(String name, String value) {
        return (Dishes) getSession().createQuery(
                "from Dishes where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Dishes getById(int id) {
        return (Dishes) getSession().load(Dishes.class, id);
    }

    public List<Menu> getMenu(int companyId) {
        List<Dishes> dishes = (List<Dishes>) getSession().createQuery(
                "from Dishes where company_id = :val")
                .setParameter("val", companyId)
                .list();

        List<Menu> menu = new ArrayList<>();

        for (Dishes dish : dishes) {
            Menu menu_item = new Menu();

            menu_item.dish = dish;

            List<DishDetails> details = _dishDetailsService.getByDishId(dish.getId());
            List<MenuDetails> menuDetails = new ArrayList<MenuDetails>();

            for (DishDetails detail : details) {
                MenuDetails menuDetail = new MenuDetails();
                menuDetail.setAmount(0);
                menuDetail.setDetail(detail);

                menuDetails.add(menuDetail);
            }

            menu_item.details = menuDetails;
            menu.add(menu_item);
        }

        return menu;
    }

    public void update(Dishes dish) {
        getSession().update(dish);
    }
}
