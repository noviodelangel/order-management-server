package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.Addresses;

import java.util.List;

@Repository
@Transactional
public class AddressesService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Addresses address) {
        return (Integer) getSession().save(address);
    }

    public void delete(Addresses address) {
        getSession().delete(address);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from Addresses").list();
    }

    public Addresses getByAttribute(String name, String value) {
        return (Addresses) getSession().createQuery(
                "from Addresses where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Addresses getAddressByCustomerId(int id)
    {
        return (Addresses) getSession().createQuery(
                "from Addresses where user_id = :id and type = 0")
                .setParameter("id", id)
                .uniqueResult();
    }

    public Addresses getAddressByUserId(int id)
    {
        return (Addresses) getSession().createQuery(
                "from Addresses where user_id = :id and type = 1")
                .setParameter("id", id)
                .uniqueResult();
    }

    public Addresses getById(int id) {
        return (Addresses) getSession().load(Addresses.class, id);
    }

    public void update(Addresses address) {
        getSession().update(address);
    }
}
