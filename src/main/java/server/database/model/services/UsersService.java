package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.Users;

import java.util.List;

@Repository
@Transactional
public class UsersService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Users user) {
         return (Integer) getSession().save(user);
    }

    public void delete(Users user) {
        getSession().delete(user);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from users").list();
    }

    public Users getByAttribute(String name, String value) {
        return (Users) getSession().createQuery(
                "from users where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Users getById(int id) {
        return (Users) getSession().load(Users.class, id);
    }

    public void update(Users user) {
        getSession().update(user);
    }
}
