package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.Customers;

import java.util.List;

@Repository
@Transactional
public class CustomersService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Customers customer) {
        return (Integer) getSession().save(customer);
    }

    public void delete(Customers customer) {
        getSession().delete(customer);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from Customers").list();
    }

    public Customers getByAttribute(String name, String value) {
        return (Customers) getSession().createQuery(
                "from Customers where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Customers getById(int id) {
        return (Customers) getSession().load(Customers.class, id);
    }

    public void update(Customers customer) {
        getSession().update(customer);
    }
}
