package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.OrderBaskets;

import java.util.List;

@Repository
@Transactional
public class OrderBasketsService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(OrderBaskets basket) {
        return (Integer) getSession().save(basket);
    }

    public void delete(OrderBaskets basket) {
        getSession().delete(basket);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from OrderBaskets").list();
    }

    public OrderBaskets getByAttribute(String name, String value) {
        return (OrderBaskets) getSession().createQuery(
                "from OrderBaskets where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public List<OrderBaskets> getByOrderId(int id) {
        return (List<OrderBaskets>) getSession().createQuery(
                "from OrderBaskets where order_id = :id")
                .setParameter("id", id).list();
    }

    public OrderBaskets getById(int id) {
        return (OrderBaskets) getSession().load(OrderBaskets.class, id);
    }

    public void update(OrderBaskets basket) {
        getSession().update(basket);
    }
}
