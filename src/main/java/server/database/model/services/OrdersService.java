package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.Orders;

import java.sql.Timestamp;
import java.util.List;

@Repository
@Transactional
public class OrdersService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(Orders order) {
         return (Integer) getSession().save(order);
    }

    public void delete(Orders order) {
        getSession().delete(order);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from Orders").list();
    }

    public Orders getByAttribute(String name, String value) {
        return (Orders) getSession().createQuery(
                "from Orders where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public Orders getFirstNotShown(int id) {
        return (Orders) getSession().createQuery(
                "from Orders where is_shown = 1 and customer_id = :val")
                .setParameter("val", id)
                .uniqueResult();
    }

    public List<Orders> getByCustomerId(int customerId) {
        List<Orders> orders = getSession().createQuery(
                "from Orders where customer_id = :id order by id desc")
                .setParameter("id", customerId)
                .list();

        return orders;
    }

    public Orders getById(int id) {
        return (Orders) getSession().load(Orders.class, id);
    }

    public void update(Orders order) {
        getSession().update(order);
    }
}
