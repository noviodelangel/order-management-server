package server.database.model.services;

/**
 * Created by sosna on 11.10.2015.
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import server.database.model.DishDetails;

import java.util.List;

@Repository
@Transactional
public class DishDetailsService {

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    public Integer save(DishDetails detail) {
        return (Integer) getSession().save(detail);
    }

    public void delete(DishDetails detail) {
        getSession().delete(detail);
    }

    @SuppressWarnings("unchecked")
    public List getAll() {
        return getSession().createQuery("from DishDetails").list();
    }

    public DishDetails getByAttribute(String name, String value) {
        return (DishDetails) getSession().createQuery(
                "from DishDetails where " + name + " = :val")
                .setParameter("val", value)
                .uniqueResult();
    }

    public List<DishDetails> getByDishId(int dishId) {
        return (List<DishDetails>) getSession().createQuery(
                "from DishDetails where dish_id = :val")
                .setParameter("val", dishId)
                .list();
    }

    public DishDetails getById(int id) {
        return (DishDetails) getSession().load(DishDetails.class, id);
    }

    public void update(DishDetails detail) {
        getSession().update(detail);
    }
}
