package server.database.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;

/**
 * Created by sosna on 12.10.2015.
 */
@Entity
@Proxy(lazy = false)
@Table(name = "order_baskets", schema = "", catalog = "ordersdb")
public class OrderBaskets {
    private int id;
    private int orderId;
    private int dishId;
    private int amount;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_id")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "dish_id")
    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    @Basic
    @Column(name = "amount")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderBaskets that = (OrderBaskets) o;

        if (id != that.id) return false;
        if (orderId != that.orderId) return false;
        if (dishId != that.dishId) return false;
        if (amount != that.amount) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + orderId;
        result = 31 * result + dishId;
        result = 31 * result + amount;
        return result;
    }
}
