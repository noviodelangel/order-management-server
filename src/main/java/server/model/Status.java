package server.model;

/**
 * Created by sosna on 11.11.2015.
 */
public enum Status {
    ORDERED(0),
    ACCEPTED(1),
    IN_PREPARATION(2),
    IN_DELIVERY(3),
    DELIVERED(4);

    int value;

    public int getValue() {
        return value;
    }

    private Status(int value)
    {
        this.value = value;
    }
}

