package server.model;

import server.database.model.Addresses;
import server.database.model.OrderBaskets;

import java.util.List;

/**
 * Created by sosna on 08.11.2015.
 */
public class Order {
    private OrderBaskets[] orderedItems;
    private Addresses address;

    public Addresses getAddress() {
        return address;
    }

    public void setAddress(Addresses address) {
        this.address = address;
    }

    public OrderBaskets[] getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(OrderBaskets[] orderedItems) {
        this.orderedItems = orderedItems;
    }
}
