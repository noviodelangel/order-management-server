package server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.Helpers.CustomerHelper;
import server.database.model.Addresses;
import server.database.model.Customers;
import server.database.model.services.AddressesService;
import server.database.model.services.CustomersService;
import server.model.CustomerWithAddress;
import server.model.Order;
import server.model.Response;

import javax.persistence.Convert;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by sosna on 11.10.2015.
 */
@RestController
public class CustomersController {

    @Autowired
    private CustomersService _customersService;

    @Autowired
    private AddressesService _addressesService;

    @RequestMapping("/api/customers/get-customer")
    @ResponseBody
    public Customers getCustomer(@RequestParam(value="id", defaultValue="1") int id,
                                 @RequestParam(value="session-number", defaultValue="") String sessionNumber) {

        try {
            if (!CustomerHelper.verify(id, sessionNumber, _customersService)) {
                throw new Exception();
            }

            return _customersService.getById(id);
        }
        catch (Exception ex) {
            return new Customers();
        }
    }

    @RequestMapping("/api/customers/add-customer")
    public Response addCustomerWithAddress(@RequestBody CustomerWithAddress customerWithAddress) {

        Response response = new Response();

        try {
            int addressId = _addressesService.save(customerWithAddress.getAddress());
            customerWithAddress.getCustomer().setAddressId(addressId);

            int customerId = _customersService.save(customerWithAddress.getCustomer());

            customerWithAddress.getAddress().setUserId(customerId);
            _addressesService.update(customerWithAddress.getAddress());

            response.setStatus(true);
            response.setMessage("Konto zostało utworzone");
        }
        catch (Exception ex){
            response.setStatus(false);
            response.setMessage("Dane są nieprawidłowe");
        }

        return response;
    }

    @RequestMapping("/api/customers/update-customer-with-address")
    public Response updateCustomerWithAddress(@RequestBody CustomerWithAddress customerWithAddress,
                                              @RequestParam(value="session-number", defaultValue="") String sessionNumber,
                                              @RequestParam(value="customer-id", defaultValue="0") String customerId) {

        Response response = new Response();

        try {
            if (!CustomerHelper.verify(Integer.parseInt(customerId), sessionNumber, _customersService)) {
                throw new Exception();
            }

            _customersService.update(customerWithAddress.getCustomer());
            _addressesService.update(customerWithAddress.getAddress());

            response.setStatus(true);
            response.setMessage("Dane zaktualizowane");
        }
        catch(Exception ex){
            response.setStatus(false);
            response.setMessage("Dane są nieprawidłowe");
        }

        return response;
    }

    @RequestMapping("/api/customers/get-all-customers")
    @ResponseBody
    public List<Customers> getAllCustomers() {
        return _customersService.getAll();
    }

    @RequestMapping("/api/customers/get-customer-address")
    @ResponseBody
    public Addresses getCustomerAddress(@RequestParam(value="id", defaultValue="1") int id,
                                        @RequestParam(value="session-number", defaultValue="") String sessionNumber) {
        try {
            if (!CustomerHelper.verify(id, sessionNumber, _customersService)) {
                throw new Exception();
            }

            return _addressesService.getAddressByCustomerId(id);
        }
        catch(Exception ex) {
            return new Addresses();
        }
    }

    @RequestMapping("/api/customers/login")
    @ResponseBody
    public Customers login(@RequestParam(value="email", defaultValue="") String email,
                           @RequestParam(value="password", defaultValue="") String password) {
        try {
            return CustomerHelper.login(email, password, _customersService);
        }
        catch(Exception ex) {
            return new Customers();
        }
    }

    @RequestMapping("/api/customers/generate-session-number")
    @ResponseBody
    public String generateSessionNumber(@RequestParam(value="email", defaultValue="") String email) {
        try {
            return CustomerHelper.generateSessionNumber(email);
        }
        catch (Exception ex) {
            return "";
        }
    }

    @RequestMapping("/api/customers/generate-password-hash")
    @ResponseBody
    public String generatePasswordHash(@RequestParam(value="password", defaultValue="") String password) {
        try {
            return CustomerHelper.hash(password);
        }
        catch(Exception ex) {
            return "";
        }
    }
}
