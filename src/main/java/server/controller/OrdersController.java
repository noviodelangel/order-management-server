package server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import server.Helpers.CustomerHelper;
import server.Helpers.OrderHelper;
import server.database.model.Addresses;
import server.database.model.OrderBaskets;
import server.database.model.Orders;
import server.database.model.services.*;
import server.model.Order;
import server.model.OrderStatus;
import server.model.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sosna on 08.11.2015.
 */
@RestController
public class OrdersController {

    @Autowired
    private OrdersService _ordersService;

    @Autowired
    private OrderBasketsService _orderBasketsService;

    @Autowired
    private AddressesService _addressService;

    @Autowired
    private CompaniesService _companiesService;

    @Autowired
    private DishDetailsService _dishDetailsService;

    @Autowired
    private DishesService _dishService;

    @Autowired
    private CustomersService _customersService;

    @RequestMapping(method=RequestMethod.POST, value="/api/orders/add-order")
    public Response addOrder(@RequestBody Order order,
                             @RequestParam(value="session-number") String sessionNumber,
                             @RequestParam(value="customerid") int customerId,
                             @RequestParam(value="companyid") int companyId) {

        Response response = new Response();
        try {
            if (!CustomerHelper.verify(customerId, sessionNumber, _customersService)) {
                throw new Exception();
            }

            OrderHelper helper = new OrderHelper(_ordersService, _orderBasketsService, _addressService, _companiesService, _dishDetailsService, _dishService);

            String message = helper.addOrder(order, customerId, companyId);
            response.setStatus(true);
            response.setMessage(message);
        }
        catch (Exception ex) {
            response.setStatus(false);
            response.setMessage("Złożenie zamówienia nie powiodło się");
        }
        return response;
    }

    @RequestMapping(method=RequestMethod.GET, value="/api/orders/get-customer-order-statuses")
         @ResponseBody
         public List<OrderStatus> getCustomerOrderStatuses(@RequestParam(value="customerId") int customerId,
                                                           @RequestParam(value="session-number", defaultValue="") String sessionNumber) {
        try {
            if (!CustomerHelper.verify(customerId, sessionNumber, _customersService)) {
                throw new Exception();
            }

            OrderHelper helper = new OrderHelper(_ordersService, _orderBasketsService, _addressService, _companiesService, _dishDetailsService, _dishService);

            List<OrderStatus> orderStatuses = helper.getCustomerOrderStatuses(customerId);

            return orderStatuses;
        }
        catch(Exception ex) {
            return new ArrayList<OrderStatus>();
        }
    }

    @RequestMapping(method=RequestMethod.GET, value="/api/orders/get-not-shown-order-status")
    @ResponseBody
    public Integer getOrderStatus(@RequestParam(value="customer-id") int customerId,
                                            @RequestParam(value="session-number", defaultValue="") String sessionNumber) {
        try {
            if (!CustomerHelper.verify(customerId, sessionNumber, _customersService)) {
                throw new Exception();
            }

            Orders order = _ordersService.getFirstNotShown(customerId);

            if (order == null)
                return -1;

            order.setIsShown(0);
            _ordersService.update(order);

            return order.getStatus();
        }
        catch(Exception ex) {
            return -1;
        }
    }
}
