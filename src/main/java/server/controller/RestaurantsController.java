package server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import server.Helpers.CustomerHelper;
import server.database.model.Companies;
import server.database.model.services.CompaniesService;
import server.database.model.services.CustomersService;
import server.database.model.services.DishDetailsService;
import server.database.model.services.DishesService;
import server.model.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sosna on 24.10.2015.
 */
@RestController
public class RestaurantsController {

    @Autowired
    private CompaniesService _companiesService;

    @Autowired
    private CustomersService _customersService;

    @RequestMapping("/api/restaurants/get-all-restaurants")
    @ResponseBody
    public List<Companies> getAllRestaurants(@RequestParam(value="session-number", defaultValue="") String sessionNumber,
                                             @RequestParam(value="customer-id", defaultValue="0") String customerId) {
        try {
            if (!CustomerHelper.verify(Integer.parseInt(customerId), sessionNumber, _customersService)) {
                throw new Exception();
            }

            return _companiesService.getAll();
        }
        catch (Exception ex) {
            return new ArrayList<Companies>();
        }
    }
}
